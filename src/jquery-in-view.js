(function() {
    "use strict";
    var lastTime = 0;
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] ||
                                      window[vendors[x]+'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame) {
        window.requestAnimationFrame = function(callback) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime + timeToCall); },
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };
    }

    if (!window.cancelAnimationFrame) {
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
    }
}());

/* global define, module */
(function (factory) {
    "use strict";
    if ( typeof define === 'function' && define.amd ) {
        // AMD. Register as an anonymous module.
        define('jquery-in-view', ['jquery'], factory);
    } else if (typeof exports === 'object') {
        // Node/CommonJS style for Browserify
        module.exports = factory;
    } else {
        // Browser globals
        factory(window.jQuery);
    }
}(function($) {
    "use strict";

    $.fn.inView = function(options) {
        options = $.extend({
            style: 'toggle',
            threshold: null,
        }, options);

        var _thresholdMatches = options.threshold ? options.threshold.match(/(\d+)(px|\%)/) : null,
            _threshold, _thresholdType;

        if (_thresholdMatches) {
            _threshold = parseInt(_thresholdMatches[1], 10);
            _thresholdType = (_thresholdMatches[2] === '%') ? 'relative' : 'absolute';
        } else {
            _threshold = 0;
            _thresholdType = 'absolute';
        }

        var callback = function() {
            this.each(function() {
                var inViewTop, inViewBottom, coverView, pastTop,
                    $el = $(this),
                    _height = $el.outerHeight(),
                    scrollTop = $(window).scrollTop(),
                    offset = $el.offset(),
                    elementTop = offset.top,
                    elementBottom = offset.top + _height,
                    _currentThreshold = _threshold;

                if (_thresholdType === 'relative') {
                    _currentThreshold = _threshold * window.innerHeight / 100;
                }

                pastTop = elementTop + _currentThreshold < scrollTop + window.innerHeight;

                inViewTop = elementTop >= scrollTop &&
                            elementTop + _currentThreshold < scrollTop + window.innerHeight;

                inViewBottom = elementBottom <= scrollTop + window.innerHeight &&
                               elementBottom - _currentThreshold > scrollTop;

                coverView = elementTop <= scrollTop && elementBottom >= scrollTop + window.innerHeight;

                if (options.style === 'toggle') {
                    $el.toggleClass('in-view--past-top', pastTop);

                    if (inViewTop && inViewBottom) {
                        if (!$el.hasClass('in-view--whole')) {
                            $el.trigger('in-view');
                            $el.addClass('in-view in-view--whole');
                        }
                    } else if (coverView) {
                        if (!$el.hasClass('in-view--cover')) {
                            $el.trigger('in-view-cover');
                            $el.addClass('in-view in-view--cover');
                        }
                    } else if (inViewTop || inViewBottom) {
                        if (!$el.hasClass('in-view--partial')) {
                            $el.trigger('in-view-partial');
                            $el.addClass('in-view in-view--partial');
                        }

                        $el.toggleClass('in-view--top', inViewTop);
                        $el.toggleClass('in-view--bottom', inViewBottom);
                        $el.removeClass('in-view--whole');
                    } else {
                        $el.removeClass('in-view in-view--whole in-view--partial');
                    }
                } else {
                    if (inViewTop && inViewBottom) {
                        if (!$el.hasClass('in-view--whole')) {
                            $el.trigger('in-view');
                        }
                        $el.addClass('in-view in-view--whole');
                    }

                    if (pastTop) {
                        $el.addClass('in-view--past-top');
                    }

                    if (coverView) {
                        if (!$el.hasClass('in-view--cover')) {
                            $el.trigger('in-view');
                        }
                        $el.addClass('in-view in-view--cover');
                    }

                    if (inViewTop || inViewBottom) {
                        if (!$el.hasClass('in-view--partial')) {
                            $el.trigger('in-view-partial');
                        }
                        $el.addClass('in-view in-view--partial');
                    }

                    if (inViewTop) {
                        $el.addClass('in-view in-view--top');
                    }

                    if (inViewBottom) {
                        $el.addClass('in-view in-view--bottom');
                    }
                }
            });
        }.bind(this);

        $(window).on('scroll.jquery-in-view', function() {
            window.requestAnimationFrame(callback);
        }).trigger('scroll.jquery-in-view');

        $(window).on('load.jquery-in-view', function() {
            window.requestAnimationFrame(callback);
        });

        return this;
    };
}));
